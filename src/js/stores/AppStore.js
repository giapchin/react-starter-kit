import __StoreBase from './__StoreBase';
import Dispatcher from '../dispatcher/Dispatcher';
import Constants from '../constants/Constants';

class AppStore extends __StoreBase {
	
	constructor() {
		super();
		
		// define private var
		Object.defineProperties(this, {
			'_counterValue': {
				value: 0,
				writable: true,
				enumerable: false
			}
		});
		
		Dispatcher.register( payload => {
			var action = payload.action;
			
			switch (action.actionType) {
				case Constants.SAMPLE_CONSTANT:
					this._counterValue++;
				break;
				
				default:
					return true;
				break;
			}
			
			this.emitChange();
			return true;
		});
	}
	
	
	get counterValue() {
		return this._counterValue;
	}
	
	
} // end class AppStore

// export singleton
export default new AppStore();
