/**
* This is a base class that is not meant to be instantiated.
*/

import { EventEmitter } from 'events';

export default class __StoreBase extends EventEmitter {
	constructor() {super(); }
	
	// communication with react components
	emitChange() { this.emit('change'); }
	addChangeListener(callback) { this.on('change', callback); }
	removeChangeListener(callback) { this.removeListener('change', callback); }
}
