import { Dispatcher as FluxDispatcher } from 'flux';

class Dispatcher extends FluxDispatcher {
	constructor(){
		super(null);
	}
	handleViewAction(action) {
		this.dispatch({
			source: 'VIEW_ACTION',
			action: action
		});
	}
}

// export singleton
export default new Dispatcher();
