import React from 'react';
import Dispatcher from '../dispatcher/Dispatcher';
import AppStore from '../stores/AppStore';
import Action from '../actions/Action';
import Constants from '../constants/Constants';

export default class SampleComponent extends React.Component {
	
	constructor(props) {
		super(props);
		
		// set default state
		this.state = this.getStates();
	}
	
	getStates(){
		return {
			value: AppStore.counterValue
		}
	}
	
	
	componentDidMount() {
		AppStore.addChangeListener( this._onChange.bind(this) );
	}
	componentWillUnmount() {
		AppStore.removeChangeListener( this._onChange.bind(this) );
	}
	
	
	handleClick(event) {
		Action.sampleAction(Constants.SAMPLE_CONSTANT);
	}
	
	render() {
		return (
			<div onClick={this.handleClick}>
				<br />
				I am SampleComponent. Click me.
				<br />
				Clicks: {this.state.value}
			</div>
		);
	} // end render
	
	
	_onChange() {
		this.setState( this.getStates() );
	}
}

