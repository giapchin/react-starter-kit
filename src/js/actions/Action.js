import Dispatcher from '../dispatcher/Dispatcher';
import Constants from '../constants/Constants';

export default class Action {
	
	static sampleAction(actionData) {
		Dispatcher.handleViewAction({
			actionType: Constants.SAMPLE_CONSTANT,
			data: actionData
		});
	}
	
}

